﻿using System;
using NUnit.Framework;
using ASPNETWebApplication.Controllers;

namespace ASPNETWebApplication2.Test
{
    public class UnitTest1
    {

        [Test]
        public void MultiplyTest_Scenario_ExpectedOutcome()
        {
            var hc = new HomeController();
            var expectedResult = 6;
            float result = hc.Multiply(2, 3);
            Assert.AreEqual(expectedResult, result);
        }

        [Test]
        public void MultiplyTest_Scenario_UnExpectedOutcome()
        {
            var hc = new HomeController();
            var expectedResult = 6;
            float result = hc.Multiply(3, 3);
            Assert.AreNotEqual(expectedResult, result);
        }

        [Test]
        public void StringTest_Scenario_ExpectedOutcome()
        {
            var hc = new HomeController();
            var expectedResult = "Hello World";
            string result = hc.concatenateString("Hello ", "World");
            Assert.AreEqual(expectedResult, result);
        }

        [Test]
        public void StringTest_Scenario_UnExpectedOutcome()
        {
            var hc = new HomeController();
            var expectedResult = "Hello World";
            string result = hc.concatenateString("Hello 2", "World");
            Assert.AreNotEqual(expectedResult, result);
        }
        [Test]
        public void MultiplyTest_Scenario_ExpectedOutcomeFAIL()
        {
            var hc = new HomeController();
            var expectedResult = 6;
            float result = hc.Multiply(2, 3);
            Assert.AreNotEqual(expectedResult, result);
        }

        [Test]
        public void MultiplyTest_Scenario_UnExpectedOutcomeFAIL()
        {
            var hc = new HomeController();
            var expectedResult = 6;
            float result = hc.Multiply(3, 3);
            Assert.AreEqual(expectedResult, result);
        }

        [Test]
        public void StringTest_Scenario_ExpectedOutcomeFAIL()
        {
            var hc = new HomeController();
            var expectedResult = "Hello World";
            string result = hc.concatenateString("Hello ", "World");
            Assert.AreNotEqual(expectedResult, result);
        }
    }
}
